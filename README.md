This project contains code necessary in reproducing the study, https://arxiv.org/abs/1902.03246, published on JHEP  10.1007/JHEP07(2019)170 (https://link.springer.com/article/10.1007%2FJHEP07%282019%29170). Together with ArgoNeuT collaboration, we carry out a first LArTPC constraints on Millicharged particles using this stratedy, https://arxiv.org/abs/1911.07996, published on PRL, Phys. Rev. Lett. 124, 131801 (2020) (https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.124.131801). 
It contains several parts:
a) event generation using Pythia for millicharged particles;
b) event processing using Mathematica notebook;
c) reach calculation using Mathematica notebook;
d) final result presentation using Mathematica notebook;


 The code is developed by Zhen Liu (zliuphys@umn.edu), owing thanks to collaborators Ivan Lepetic, Roni Harnik, and Ornella Palamara. Please feel free to contact me for questions. For people using this code, please feel free to request merge and update it and you can become contributor.
 The code is not well-documented but through the course of Open science, we can make it better.
